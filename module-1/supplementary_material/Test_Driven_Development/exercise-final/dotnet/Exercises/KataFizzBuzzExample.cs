﻿using System;

namespace Exercises
{
    class KataFizzBuzzExample
    {
        static void Main(string[] args)
        {
            KataFizzBuzz kfb = new KataFizzBuzz();
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(kfb.FizzBuzz(i));
            }

            Console.ReadLine();
        }
    }
}
