﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class KataFizzBuzzTests
    {
        [TestMethod]
        public void FizzBuzz_0_Test()
        {
            KataFizzBuzz kfb = new KataFizzBuzz();
            Assert.AreEqual("", kfb.FizzBuzz(0));
        }

        [TestMethod]
        public void FizzBuzz_101_Test()
        {
            KataFizzBuzz kfb = new KataFizzBuzz();
            Assert.AreEqual("", kfb.FizzBuzz(101));
        }

        [TestMethod]
        public void FizzBuzz_Obvious_Cases_Test()
        {
            KataFizzBuzz kfb = new KataFizzBuzz();
            Assert.AreEqual("Fizz", kfb.FizzBuzz(3));
            Assert.AreEqual("Buzz", kfb.FizzBuzz(5));
            Assert.AreEqual("Fizz", kfb.FizzBuzz(6));
            Assert.AreEqual("Fizz", kfb.FizzBuzz(9));
            Assert.AreEqual("FizzBuzz", kfb.FizzBuzz(15));
            Assert.AreEqual("FizzBuzz", kfb.FizzBuzz(45));
        }

        [TestMethod]
        public void FizzBuzz_Non_Fizzy_Buzzy_Numbers_Test()
        {
            KataFizzBuzz kfb = new KataFizzBuzz();
            Assert.AreEqual("2", kfb.FizzBuzz(2));
            Assert.AreEqual("7", kfb.FizzBuzz(7));
            Assert.AreEqual("17", kfb.FizzBuzz(17));
            Assert.AreEqual("Fizz", kfb.FizzBuzz(34)); //Step 2: 34 contains a 3, so is now Fizz, not 34
            Assert.AreEqual("Buzz", kfb.FizzBuzz(53)); //Step 2: 53 contains a 5 and 3, but 5 is checked first, so Buzz
            Assert.AreEqual("86", kfb.FizzBuzz(86));
        }
    }
}
