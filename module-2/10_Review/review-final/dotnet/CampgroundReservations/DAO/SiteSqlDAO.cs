﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CampgroundReservations.Models;

namespace CampgroundReservations.DAO
{
    public class SiteSqlDAO : ISiteDAO
    {
        private readonly string connectionString;

        public SiteSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        public IList<Site> GetSitesThatAllowRVs(int parkId)
        {
            List<Site> sites = new List<Site>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT site_id, s.campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities FROM site s "
                                                        + "INNER JOIN campground c ON c.campground_id = s.campground_id "
                                                        + "INNER JOIN park p ON p.park_id = c.park_id "
                                                        + "WHERE max_rv_length > 0 "
                                                        + "AND p.park_id = @parkId", conn);
                    cmd.Parameters.AddWithValue("@parkId", parkId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Site site = GetSiteFromReader(reader);
                        sites.Add(site);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error occurred getting sites that allow RVs");
                Console.WriteLine(ex.Message);
                throw;
            }

            return sites;
        }

        public IList<Site> GetAvailableSites(int parkId)
        {
            List<Site> sites = new List<Site>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT site_id, s.campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities "
                                                        + "FROM site s "
                                                        + "INNER JOIN campground c ON c.campground_id = s.campground_id "
                                                        + "WHERE park_id = @parkId "
                                                        + "AND site_id NOT IN ( "
                                                            + "SELECT site_id FROM reservation "
                                                            + "WHERE GETDATE() BETWEEN from_date AND to_date)", conn);
                    cmd.Parameters.AddWithValue("@parkId", parkId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Site site = GetSiteFromReader(reader);
                        sites.Add(site);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error occurred getting sites that allow RVs");
                Console.WriteLine(ex.Message);
                throw;
            }

            return sites;
        }

        public IList<Site> GetAvailableSites(int parkId, DateTime startDate, DateTime endDate)
        {
            List<Site> sites = new List<Site>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT site_id, s.campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities "
                                                        + "FROM site s "
                                                        + "INNER JOIN campground c ON c.campground_id = s.campground_id "
                                                        + "WHERE park_id = @parkId "
                                                        + "AND site_id NOT IN ( "
                                                            + "SELECT site_id FROM reservation "
                                                            + "WHERE @startDate BETWEEN from_date AND to_date "
                                                            + "OR @endDate BETWEEN from_date AND to_date)", conn);
                    cmd.Parameters.AddWithValue("@parkId", parkId);
                    cmd.Parameters.AddWithValue("@startDate", startDate);
                    cmd.Parameters.AddWithValue("@endDate", endDate);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Site site = GetSiteFromReader(reader);
                        sites.Add(site);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error occurred getting sites that allow RVs");
                Console.WriteLine(ex.Message);
                throw;
            }

            return sites;
        }

        private Site GetSiteFromReader(SqlDataReader reader)
        {
            Site site = new Site();
            site.SiteId = Convert.ToInt32(reader["site_id"]);
            site.CampgroundId = Convert.ToInt32(reader["campground_id"]);
            site.SiteNumber = Convert.ToInt32(reader["site_number"]);
            site.MaxOccupancy = Convert.ToInt32(reader["max_occupancy"]);
            site.Accessible = Convert.ToBoolean(reader["accessible"]);
            site.MaxRVLength = Convert.ToInt32(reader["max_rv_length"]);
            site.Utilities = Convert.ToBoolean(reader["utilities"]);

            return site;
        }
    }
}
