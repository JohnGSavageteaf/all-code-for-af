# Module 3 - Web Application Development

Module 3 covers an introduction to HTML and CSS, and JavaScript. Students learn the fundamentals of HTML and CSS in week one, and then learn about the fundamentals of JavaScript in week two. During week three, students learn about a framework called VueJs that you can use to build applications.

At the end of the module, students learn how to write a web service client using JavaScript.

**The ID for the day's Daily Pulse Survey is: SOC-39127023.**

### Week 1 ###
| Lecture | Java Quiz | .NET Quiz |
| ------| -------- | -------- |
| [Intro to HTML and CSS](01_Intro_HTML_CSS/lecture-notes/README.md) | SOC-48686969 | SOC-48686969 |
| [CSS Selectors & Layouts](02_CSS_Selectors/lecture-notes/README.md) | SOC-48585487 | SOC-48585487 |
| [CSS Grid](03_CSS_Grid/lecture-notes/README.md) | SOC-48792322 | SOC-48792322 |
| [Flexbox](04_Flexbox/lecture-notes/README.md) | SOC-48587456 | SOC-48587456 |
| **Week 1 Review** | N/A | N/A |

### Week 2 ###
| Lecture | Java Quiz | .NET Quiz |
| ------| -------- | -------- |
| [Introduction to JavaScript](06_Introduction_to_JavaScript/lecture-notes/README.md) | SOC-43139783 | SOC-43139783 |
| [JavaScript Functions](07_JavaScript_Functions/lecture-notes/README.md) | SOC-40961152 | SOC-40961152 |
| [DOM](08_DOM/lecture-notes/README.md) | SOC-40961229 | SOC-40961229 |
| [Event Handling](09_Event_Handling/lecture-notes/README.md) | SOC-40961295 | SOC-40961295 |
| **Week 2 Review** | N/A | N/A |

### Week 3 ###
| Lecture | Java Quiz | .NET Quiz |
| ------| -------- | -------- |
| [Introduction to Vue](11_Introduction_to_Vue_and_Data_Binding/lecture-notes/README.md) | SOC-48590007 | SOC-48590007 |
| [Vue Event Handling](12_Vue_Event_Handling/lecture-notes/README.md) | SOC-48922044 | SOC-48922044 |
| [Vue Component Communication](13_Vue_Component_Communication/lecture-notes/README.md) | SOC-48591297 | SOC-48591297 |
| **Week 3 Review** | N/A | N/A |
| Matchmaking | N/A | N/A |

### Week 4 ###
| Lecture | Java Quiz | .NET Quiz |
| ------| -------- | -------- |
| [Vue Routing](15_Vue_Routing/lecture-notes/README.md) | SOC-48591666 | SOC-48591666 |
| [Web Services (GET)](16_Web_Services_GET/lecture-notes/README.md) | SOC-48591912 | SOC-48591912 |
| [Web Services (POST) and Error Handling](17_Web_Services_POST/lecture-notes/README.md) | SOC-48592101 | SOC-48592101 |
| **Week 4 Review** | N/A | N/A |
| Matchmaking | N/A | N/A |